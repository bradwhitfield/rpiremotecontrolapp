# README #

### Building a Raspberry Pi Car ###

* Using a Raspberry Pi, an Android app, Bluetooth, and various hardware.
* 0.9
* [Download APK here](http://107.161.160.219/rpiapp)
* Requires a device with Android 4.1 Jelly Bean, a Bluetooth adapter, and an accelerometer

### Install the APK ###

* Download on the device the APK from the link above
* Go into settings on your device
* Go to security settings
* Check Allow apps from Unknown Sources
* Open the Downloads app
* Select the APK you just downloaded
* Touch Install

### Compile from Source ###

* Requires Android Studio or Intellij
* Compiled with SDK version 20
* Minimum SDK version is 16
* Download the source code or clone the repo using git

### More Info Coming Soon ###