package com.cs432.rpiremotecontrol.remoteapplication;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class Remote extends Activity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private long lastUpdateTime = 0;
    private static final int INTERVAL_RATE = 500;
    private int instruction = 0;
    private boolean rightSigOn = false;
    private boolean leftSigOn = false;
    private boolean accelStarted = false;

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (accelStarted){                                         // checking for this causes data to not show up right away????
            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdateTime) > INTERVAL_RATE) {
                lastUpdateTime = curTime;

                float x = event.values[0];
                float y = event.values[1];

                //these if statements determine which way to move

                if(Math.abs(x) > 2.7 || Math.abs(y) > 2.7) { //indicates how much you have to tilt for movement
                    if (Math.abs(x) > Math.abs(y)) { //check to see which way it is tilted more
                        //move forward or back
                        if( x < 0) { //if x is negative
                            goForward(findViewById(R.id.xyTxt));
                        } else {
                            goBackward(findViewById(R.id.xyTxt));
                        }
                    } else {
                        //move left or right
                        if( y < 0) { //if y is negative
                            goLeft(findViewById(R.id.xyTxt));
                        } else {
                            goRight(findViewById(R.id.xyTxt));
                        }
                    }
                } else { //if it is not tilted enough, stop the car
                    resetDirection();
                }
                TextView t = (TextView) findViewById(R.id.xyTxt);
                t.setText("X: " + (int) x + "\nY: " + (int) y);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().getDecorView().setBackgroundColor(Color.DKGRAY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.remote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_exit_remote) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void goLeft(View view) {
        instruction = 4;
        move();
    }

    public void goRight(View view){
        instruction = 6;
        move();
    }

    public void goForward(View view){
        instruction = 8;
        move();
    }

    public void goBackward(View view){
        instruction = 2;
        move();
    }

    public void move(){

        TextView t = (TextView) findViewById(R.id.directionTxt);
        t.setText("Direction: " + instruction);
        // move car here
     //   resetDirection();
    }

    private void resetDirection() { //reset the instruction
        Handler mHandler = new Handler();

        mHandler.postDelayed(new Runnable() {
            public void run() {
                instruction = 0;
                TextView t = (TextView) findViewById(R.id.directionTxt);
                t.setText("Direction: " + instruction);
            }
        }, 1000);
        //brake lights here, stop the car
    }

    public void rightSig(View view) throws InterruptedException {
        if (rightSigOn) {
            instruction = 9;
            //send signal to turn OFF right blinker
            findViewById(R.id.rightSigBtn).setBackgroundColor(Color.GRAY);
            rightSigOn = false;
        } else {
            //send signal to turn ON right blinker
            instruction = 9;
            findViewById(R.id.rightSigBtn).setBackgroundColor(Color.GREEN);
            rightSigOn = true;
        }
    }

    public void leftSig(View view) throws InterruptedException {
        if (leftSigOn) {
            instruction = 7;
            //send signal to turn OFF left blinker
            findViewById(R.id.leftSigBtn).setBackgroundColor(Color.GRAY);
            leftSigOn = false;
        }
        else {
            instruction = 7;
            //send signal to turn ON left blinker
            findViewById(R.id.leftSigBtn).setBackgroundColor(Color.GREEN);
            leftSigOn = true;
        }
    }

    public void toggleGlow(View view){
        if (((ToggleButton)view).isChecked()){
            instruction = 0;
            //send signal to turn ON underglow
            findViewById(R.id.glowBtn).setBackgroundColor(Color.BLUE);
        }
        else {
            instruction = 0;
            //send signal to turn OFF underglow
            (findViewById(R.id.glowBtn)).setBackgroundColor(Color.GRAY);
        }
    }

    public void toggleHeadLights(View view){
        if (((ToggleButton)view).isChecked()){
            instruction = 1;
            //send signal to turn ON headlights
            findViewById(R.id.hLightsBtn).setBackgroundColor(Color.YELLOW);
        }
        else {
            instruction = 1;
            //send signal to turn OFF underglow
            (findViewById(R.id.hLightsBtn)).setBackgroundColor(Color.GRAY);
        }
    }

    public void startStop(View view) throws InterruptedException {
        if (!accelStarted) {  //if it is not already running, start it
            findViewById(R.id.startStopBtn).setBackgroundColor(Color.RED);
            Button b = (Button) findViewById(R.id.startStopBtn);
            b.setText("Stop Accelerometer");
            findViewById(R.id.xyTxt).setVisibility(View.VISIBLE);
            accelStarted = true;
            Toast.makeText(this, "Accelerometer Started!", Toast.LENGTH_SHORT).show(); //might cause accel data delay?
        } else { //if it is running, stop it
            findViewById(R.id.startStopBtn).setBackgroundColor(Color.GREEN);
            findViewById(R.id.xyTxt).setVisibility(View.INVISIBLE);
            accelStarted = false;
            Button b = (Button) findViewById(R.id.startStopBtn);
            b.setText("Start Accelerometer");
            Toast.makeText(this, "Accelerometer Stopped!", Toast.LENGTH_SHORT).show();
        }
    }

}
